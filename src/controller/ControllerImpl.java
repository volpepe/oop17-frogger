package controller;

import model.GameDifficult;
import model.GameLogic;
import utilities.Constants;
import view.View;

/**
 * controller implementation.
 */
public class ControllerImpl implements Controller {

    private final LeaderboardManager lm;
    private final View view;
    private GameLoop gameLoop;
    private int fps = Constants.FPS_DEFAULT;
    private GameDifficult diff = Constants.DIFF_DEFAULT;
    private final GameLogic game;

    /**
     * @param v View
     * @param g GameLogic
     */
    public ControllerImpl(final View v, final GameLogic g) {
        view = v;
        game = g;
        lm = new LeaderboardManagerImpl();
    }

    /**
     * start the game loop.
     */
    public void startGame() {
        gameLoop = new GameLoop(fps, view, diff, game, lm);
        gameLoop.startGameLoop();
    }

    /**
     * stop the game.
     */
    public void stopGame() {
        if (gameLoop != null && gameLoop.isActive()) {
            gameLoop.stopGameLoop();
        }
    }

    /**
     * pause the game.
     */
    public void pauseGame() {
        gameLoop.pauseGameLoop();
    }

    /**
     * resume the game from pause.
     */
    public void resumeGame() {
        gameLoop.resumeGameLoop();
    }

    /**
     * retrieve the score.
     * @return score list
     */
    public LeaderboardManager getLeaderboardManager() {
        return lm;
    }

    /**
     * set the difficult.
     * @param d difficult
     */
    public void setDifficult(final GameDifficult d) {
        diff = d;
    }

    /**
     * set the FPS.
     * @param fps to be displayed
     */
    public void setFPS(final int fps) {
       this.fps = fps;
    }

    /**/
    @Override
    public GameLogic getGameLogic() {
        return this.game;
    }
}
