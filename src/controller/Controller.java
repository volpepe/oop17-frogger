package controller;

import model.GameDifficult;
import model.GameLogic;

/**
 * public interface for controller.
 */
public interface Controller {
    /**
     * start the game loop.
     */
    void startGame();

    /**
     * stop the game loop.
     */
    void stopGame();

    /**
     * pause game loop.
     */
    void pauseGame();

    /**
     * resume game from pause state.
     */
    void resumeGame();

    /**
     * get leaderboard manager object.
     * @return leaderboard manager
     */
    LeaderboardManager getLeaderboardManager();
    /**
     * set the difficult.
     * @param diff 
     */
    void setDifficult(GameDifficult diff);
    /**
     * set fps option.
     * @param fps to be execute from gameloop.
     */
    void setFPS(int fps);
    /**
     * Getter for the GameLoop.
     * @return gameLoop
     */
    GameLogic getGameLogic();

}
