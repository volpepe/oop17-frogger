package controller.movement;

import model.frog.Frog;
import utilities.Constants;
import utilities.Direction;
/**
 * 
 */
public class MoveDown implements Command {

    /**
     * Moves the frog based on the first key in the list. WASD used as directional keys.
     * @param frog the frog game object to move
     */

    public void execute(final Frog frog) {

        if (frog.getOccupiedLane() - frog.getVerticalMovementValue() >= 0
                && frog.getOccupiedLane() - frog.getVerticalMovementValue() < Constants.WORLD_NUMBER_OF_LANE) {
            frog.setFacing(Direction.FACING_DOWN);
            frog.moveTo(frog.getOccupiedLane() - frog.getVerticalMovementValue(), frog.getCenter());
        }
    }
}
