package controller;

import java.util.List;

import utilities.Pair;

/**
 * LeaderboardManager interface.
 */
public interface LeaderboardManager {
    /**
     * retrieve the score list.
     * @return score list unmodifiable.
     */
    List<Pair<String, Integer>> getScoreList();

    /**
     * 
     * @param score to add.
     */
    void addScore(Pair<String, Integer> score);

    /**
     * empyty the score record.
     */
    void resetAllScore();

    /**
     * update the memorize score to permanent destination.
     */
    void update();
    /**
     * check if the new score is in the top N score.
     * @param s new score to check
     * @return true if could be added in list, false otherwise
     */
    boolean checkScore(int s);
    /**
     * get the last score added after ended the game.
     * @return last score.
     */
    int getPartialScore();
    /**
     * set point obtained after gameover.
     * @param points for new score
     */
    void setPartialScore(int points);
}
