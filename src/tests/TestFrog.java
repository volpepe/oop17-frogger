package tests;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import model.frog.Frog;
import model.gameobject.GameObjectImpl.GameObjectType;
import model.world.World;
import model.world.WorldImpl;
import utilities.Constants;

/**
 * JUnit test for Frog creation and movements.
 */
public class TestFrog {

    private static final String MESSAGE_TRUE = "Should be the true";

    private final World world = new WorldImpl.Builder().addSafeLane()
            .addStreet(2, GameObjectType.PURPLE_CAR, 4)
            .addStreet(4, GameObjectType.RACE_CAR, 2)
            .addStreet(2, GameObjectType.WHITE_CAR, 3)
            .addStreet(2, GameObjectType.TRUCK, 3)
            .addStreet(2, GameObjectType.TRUCK, 3)
            .addSafeLane()
            .addRiver(3, GameObjectType.SMALL_LOG, 4)
            .addRiver(2, GameObjectType.MEDIUM_LOG, 2)
            .addRiver(4, GameObjectType.SMALL_LOG, 4)
            .addRiver(3, GameObjectType.BIG_LOG, 1)
            .addRiver(4, GameObjectType.SMALL_LOG, 4)
            .addEndLane()
            .build();

    /**
     * Test if frog creation criteria are met.
     */
    @Test
    public void testFrogCreation() {

        try {
            @SuppressWarnings("unused")
            final Frog frog = new Frog(world, 0, 0, GameObjectType.TURTLE.create(0));
            fail("Can create frog with objType different than FROG");
        } catch (IllegalArgumentException e) {
            System.out.println("Can't create frog with objType different than FROG");
        }

        try {
            @SuppressWarnings("unused")
            final Frog frog = new Frog(world, -1, 0, GameObjectType.FROG.create(0));
            fail("Object can be moved on a non-exixting lane");
        } catch (Exception e) {
            System.out.println("Object cannot be moved on a non-exixting lane");
        }
    }

    /**
     * Test if frog movements respect world bounds.
     */
    @Test
    public void testFrogMovements() {

        final Frog frog = new Frog(world, 0, 0, GameObjectType.FROG.create(0));

        //Check successful in bound movement.
        frog.moveTo(0, Constants.WORLD_RIGHT_LIMIT);
        assertTrue(MESSAGE_TRUE, frog.getCenter() == Constants.WORLD_RIGHT_LIMIT && frog.getOccupiedLane() == 0);

        frog.moveTo(0, Constants.WORLD_LEFT_LIMIT);
        assertTrue(MESSAGE_TRUE, frog.getCenter() == Constants.WORLD_LEFT_LIMIT && frog.getOccupiedLane() == 0);

        frog.moveTo(0, 0);
        assertTrue(MESSAGE_TRUE, frog.getCenter() == 0 && frog.getOccupiedLane() == 0);

        frog.moveTo(Constants.WORLD_NUMBER_OF_LANE, 0);
        assertTrue(MESSAGE_TRUE, frog.getCenter() == 0 && frog.getOccupiedLane() == Constants.WORLD_NUMBER_OF_LANE);

        //Check that out of bound movement is not successful
        frog.moveTo(0, 0); //Reset to known position
        try {
            frog.moveTo(0, Constants.WORLD_RIGHT_LIMIT + 1);
            fail("Can move object out of bounds, right");
        } catch (IllegalArgumentException e) {
            System.out.println("Cannot move object out of bounds, right");
        }

        frog.moveTo(0, 0); //Reset to known position
        try {
            frog.moveTo(0, Constants.WORLD_LEFT_LIMIT - 1);
            fail("Can move object out of bounds, left");
        } catch (IllegalArgumentException e) {
            System.out.println("Cannot move object out of bounds, left");
        }

        frog.moveTo(0, 0); //Reset to known position
        try {
            frog.moveTo(-1, 0);
            fail("Can move object out of bounds, down");
        } catch (IllegalArgumentException e) {
            System.out.println("Cannot move object out of bounds, down");
        }

        frog.moveTo(0, 0); //Reset to known position
        try {
            frog.moveTo(Constants.WORLD_NUMBER_OF_LANE + 1, 0);
            fail("Can move object out of bounds, up");
        } catch (IllegalArgumentException e) {
            System.out.println("Cannot move object out of bounds, up");
        }

    }

}
