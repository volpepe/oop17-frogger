package model.score;

import model.GameDifficult;

/**
 * This interface manages score.
 * Score increase when frog moves up or reaches den.
 * Score decrease when frog dies.
 */
public interface ScoreManager {

    /**
     * @param difficult of the game.
     */
    void setDifficult(GameDifficult difficult);

    /**
     * @return the actual score.
     */
    int getScore();

    /**
     * @param score to add.
     */
    void addScore(int score);

    /**
     * Method called to update the score.
     * @param frogLane where frog is.
     */
    void update(int frogLane);

    /**
     * 
     */
    void frogDies();

    /**
     * reset the current score.
     */
    void resetScore();

}
