package model;
/**
 * game difficult.
 */
public enum GameDifficult {
    /**
     * easy mode.
     */
    EASY(0.7, 0.5, 1.5),
    /**
     * normal mode.
     */
    NORMAL(1, 0.8, 1),
    /**
     * hard mode.
     */
    HARD(1.5, 1, 0.8);

    private double speedMultiplier;
    private double roadObstacleMultiplier;
    private double riverObstacleMultiplier;

    GameDifficult(final double speedMulti, final double roadObstacleMulti, final double riverObstacleMulti) {
        this.speedMultiplier = speedMulti;
        this.roadObstacleMultiplier = roadObstacleMulti;
        this.riverObstacleMultiplier = riverObstacleMulti;
    }
    /**
     * get the speed multiplier.
     * @return speed multiplier
     */
    public double getSpeedMultiplier() {
        return speedMultiplier;
    }
    /**
     * get the road number obstacle multiplier.
     * @return road multiplier
     */
    public double getRoadObstacleMultiplier() {
        return roadObstacleMultiplier;
    }
    /**
     * get the river number obstacle multiplier.
     * @return river multiplier
     */
    public double getRiverObstacleMultiplier() {
        return riverObstacleMultiplier;
    }


}
