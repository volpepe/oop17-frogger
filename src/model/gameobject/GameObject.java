package model.gameobject;

import model.gameobject.GameObjectImpl.GameObjectType;

/**
 * This interface represents an object in the world.
 */
public interface GameObject {

    /**
     * 
     * @return the center of the object.
     */
    double getCenter();

    /**
     * 
     * @param newCenter is the new center of the object.
     */
    void setCenter(double newCenter);

    /**
     * 
     * @return the type of the object.
     */
    GameObjectType getGameObjectType();

    /**
     * 
     * @return the width of the object.
     */
    double getWidth();

    /**
     * 
     * @return the base type of the object.
     */
    ObstacleBaseType getBaseType();

    /**
     * Returns true if this Game Object and the one provided are colliding.
     * @param gameObject the Game Object to use for the collision check.
     * @return true if the objects collide, false otherwise
     */
    default boolean isColliding(GameObject gameObject) {
        return (!(gameObject.getCenter() + gameObject.getWidth() / 2 <= this.getCenter() - this.getWidth() / 2
                || gameObject.getCenter() - gameObject.getWidth() / 2 >= this.getCenter() + this.getWidth() / 2));
    }

}
