package model.lane;

/**
 * LaneType is the type of the lane.
 */
public enum LaneType {

    /**
     * A lane with no obstacles where the frog can land safely.
     */
    SAFE_LANE,

    /**
     * A lane where vehicles are spawned.
     */
    STREET,

    /**
     * A lane where logs and turtle are spawned.
     */
    RIVER,

    /**
     * The end lane.
     */
    END_LANE;

}
