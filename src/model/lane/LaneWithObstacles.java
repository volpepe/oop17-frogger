package model.lane;

import java.util.LinkedList;
import java.util.List;

import model.gameobject.GameObject;
import model.gameobject.GameObjectImpl.GameObjectType;
import utilities.Constants;

/**
 * This class is a specialization of class AbstractLaneWithMod.
 * This class represents lanes that have obstacles.
 */
public class LaneWithObstacles extends AbstractLane {

    private static final String EXCEPTION_MESSAGE = "LanwWithObstacle can't be safe lane or end lane";
    private final List<GameObject> obstacleSet;
    private double speed;

    /**
     * @param obstacleSet lane will have.
     * @param speed lane will have.
     * @param type of lane.
     */
    public LaneWithObstacles(final List<GameObject> obstacleSet, final double speed, final LaneType type) {
        super(type);
        if (type == LaneType.SAFE_LANE || type == LaneType.END_LANE) {
            throw new IllegalArgumentException(EXCEPTION_MESSAGE);
        }
        this.obstacleSet = obstacleSet;
        this.speed = speed;
    }

    /**
     *
     */
    @Override
    public void update() {
        this.obstacleSet.forEach(o -> {
            o.setCenter(this.calculateUpdatePosition(o.getGameObjectType(), o.getCenter()));
        });
    }

    /**
     * This method return a defensive copy of obstacles in lane.
     * @return obstacles in the lane.
     */
    @Override
    public List<GameObject> getObstacle() {
        return new LinkedList<>(this.obstacleSet);
    }

    /**
     * 
     */
    @Override
    public double getSpeed() {
        return this.speed;
    }

    /**
     * 
     */
    @Override
    public void setSpeed(final double speed) {
        this.speed = speed;
    }

    private double calculateUpdatePosition(final GameObjectType obstacleType, final double center) {
        if (center >= (Constants.WORLD_RIGHT_LIMIT + obstacleType.getWidth() / 2) && this.speed > 0) {
            return Constants.WORLD_LEFT_LIMIT - obstacleType.getWidth() / 2;
        } else if (center <= (Constants.WORLD_LEFT_LIMIT - obstacleType.getWidth() / 2) && this.speed < 0) {
            return Constants.WORLD_RIGHT_LIMIT + obstacleType.getWidth() / 2;
        } else {
           return center + this.speed;
        }
    }
}
