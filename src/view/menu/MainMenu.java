package view.menu;

import java.util.stream.Collectors;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import view.AbstractScene;
import view.View;
import view.ViewImpl;
import view.ViewImpl.GameScreen;
import view.game.GameScene;

/**
 * Layout for main menu.
 */
public class MainMenu extends AbstractScene {

    private static final double BUTTON_WIDTH = 3.0;
    private static final int IMAGE_POS = 10;
    private static final int TRANSLATE_X = 35;
    private static final int BOX_SPACING = 15;
    /**
     * @param width The width of the stage.
     * @param height The height of the stage.
     * @param v view
     */
    public MainMenu(final double width, final double height, final View v) {
        super(new BorderPane(), width, height);
        final View view = v;
        final ImageView image = new ImageView();
        final BorderPane pane = (BorderPane) getRoot();
        final Button start = new Button("START GAME");
        final Button options = new Button("OPTIONS");
        final Button leaderboard = new Button("LEADERBOARD");
        final Button exit = new Button("EXIT");
        final Button credits = new Button("CREDITS");
        final Button rules = new Button("INSTRUCTIONS");
        final VBox box = new VBox(start, options, leaderboard, rules, exit);
        start.prefWidthProperty().bind(this.widthProperty().divide(BUTTON_WIDTH));
        options.prefWidthProperty().bind(this.widthProperty().divide(BUTTON_WIDTH));
        leaderboard.prefWidthProperty().bind(this.widthProperty().divide(BUTTON_WIDTH));
        exit.prefWidthProperty().bind(this.widthProperty().divide(BUTTON_WIDTH));
        credits.prefWidthProperty().bind(this.widthProperty().divide(BUTTON_WIDTH));
        rules.prefWidthProperty().bind(this.widthProperty().divide(BUTTON_WIDTH));
        image.setId("menuImage");
        image.setFitHeight(height / IMAGE_POS);
        image.setFitWidth(width);
        image.setTranslateY(height / IMAGE_POS);
        image.setTranslateX(width / TRANSLATE_X);
        box.setSpacing(height / BOX_SPACING);
        box.setAlignment(Pos.CENTER);
        box.setId("main-menu-box");
        pane.setTop(image);
        BorderPane.setAlignment(image, Pos.CENTER);
        pane.setCenter(box);
        pane.setBottom(credits);
        BorderPane.setAlignment(credits, Pos.BOTTOM_RIGHT);

        exit.setOnAction(e -> Platform.exit());
        options.setOnAction(e -> view.changeScene(ViewImpl.GameScreen.OPTIONS));
        leaderboard.setOnAction(e ->  view.changeScene(ViewImpl.GameScreen.LEADERBOARD).initialize());
        start.setOnAction(e -> {
            final GameScene s = (GameScene) view.changeScene(GameScreen.GAME);
            Platform.runLater(() -> s.initialize(v.getController().getGameLogic().getWorld()
                                        .getLane().stream()
                                        .map(l -> l.getLaneType())
                                        .collect(Collectors.toList())));
            view.getController().startGame();
        });
        credits.setOnAction(e -> {
            final Alert creditsAlert = new Alert(AlertType.NONE);
            final DialogPane dialPane = creditsAlert.getDialogPane();
            dialPane.getStylesheets().add(getClass().getResource("/style.css").toExternalForm());
            dialPane.setMinHeight(Region.USE_PREF_SIZE);
            dialPane.setMinWidth(Region.USE_PREF_SIZE);
            dialPane.getButtonTypes().add(new ButtonType("CLOSE"));
            dialPane.styleProperty().bind(Bindings.format("-fx-font-size: %.2fpt;", super.getFontSize()));
            creditsAlert.setTitle("CREDITS");
            creditsAlert.setHeaderText("AUTHORS");
            creditsAlert.setContentText("Bonoli Mattia\nCichetti Federico\nPistocchi Filippo\nSponziello Nicolo");
            creditsAlert.showAndWait();
        });
        rules.setOnAction(e -> {
            final Alert rulesAlert = new Alert(AlertType.NONE);
            final DialogPane dialPane = rulesAlert.getDialogPane();
            dialPane.getStylesheets().add(getClass().getResource("/style.css").toExternalForm());
            dialPane.setMinHeight(Region.USE_PREF_SIZE);
            dialPane.getButtonTypes().add(new ButtonType("CLOSE"));
            dialPane.styleProperty().bind(Bindings.format("-fx-font-size: %.2fpt;", super.getFontSize()));
            rulesAlert.setTitle("INSTRUCTION");
            rulesAlert.setHeaderText("Commands:\nWASD to move the frog\nF1 to enter GOD MODE (no collisions)");
            rulesAlert.setContentText("Try to avoid cars and get to the safe "
                    + "and warm home! Along the path you may find "
                    + "some strange \"things\", it's up to you to "
                    + "decide if it's worth taking them!");
            rulesAlert.showAndWait();
        });
    }
    @Override
    public void initialize() {   }
}
