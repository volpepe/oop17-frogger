package view.game;

import javafx.scene.image.Image;

/**
 * Interface for generic LaneView.
 */
public interface LaneView {

    /**
     * The prepare method is void and is called every time something needs to be drawn. 
     * It clears the lane from the drawings of all the previous Obstacles.
     */
    void prepare();

    /**
     * This method draws the correct entities in the correct position upon the lane.
     * @param width The model-width of the entity to be drawn.
     * @param position The position of the top-left corner of the rectangle that contains the drawing of the entity.
     * @param img The texture of the entity to draw on the lane.
     * @param angle The rotation degrees this image will be drawn with.
     */
    void drawEntity(Image img, Double position, Double width,
            double angle);
}
