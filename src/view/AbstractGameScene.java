package view;

import java.util.List;

import javafx.scene.Parent;
import model.lane.LaneType;
/**
 * template class for game scene.
 */
public abstract class AbstractGameScene extends AbstractScene {

    /**
     * 
     * @param root The root of this scene.
     * @param width The width of this scene.
     * @param height The height of this scene.
     */
    public AbstractGameScene(final Parent root, final double width, final double height) {
        super(root, width, height);
    }

    /**
     * empty implementation.
     */
    public void initialize() {    }

    /**
     * scene initialization with arguments.
     * @param lanes The list of lanes to draw.
     */
    public abstract void initialize(List<LaneType> lanes);

}
